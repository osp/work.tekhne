Here is the initial graph flowchart thingy for the website

```mermaid
flowchart TD
    subgraph "Navigation"
    subgraph "Blog page"
    DefaultBlog["`Default Blog
      a page that lists blog posts
      Filters:
      - Partners
      - Categories
    `"]
    Qo2Blog["`Qo2 Blog (filtered by partner)
      - A page that lists all blog posts by Qo2
      - A short bio about Qo2
    `"]
    RadioBlog["`Radio Blog (filtered by category)
      a page that lists all blog posts related to the category
      Includes a short blurb about the category(?)
    `"]
    end
    subgraph "Calendar page"
    DefaultCalendar["`Default Calendar
      - A page that lists calendar events
      Filters:
      - Partners
      - Categories
    `"]
    Qo2Calendar["`Qo2 Calendar
      - A page that lists calendar events only by Qo2
      - A short bio about Qo2
    `"]
    YearCalendar["`2022 Calendar
      - A page that lists calendar events only from 2022
    `"]
    end
    About["`About
      - A page with a short text that describes the project
      - A short bio for each of the six partners
    `"]
    Artists["`Artists
    - A page that lists all the artists connected to the project
    `"]
    end

    subgraph "Dynamic Pages"

    subgraph "Qo2 Radio blog post"
    BlogPost["`Blog post
    - A single blog post containing something
    - Has an author / author organization
    - Can have linked artist(s)
    - Can have linked event(s)
    - Can have Echo(s)/link(s) to other blog posts
    `"]
    AuthorQo2{{"Author: Qo2"}}
    CategryRadio{{"Category: Radio"}}
    BlogArtist{{"Artists: Some artist 1"}}
    end

    subgraph "Qo2 2022 Event"
    Event["`Event
    - A single evebt containing something
    - Has an author / author organization
    - Can have linked artist(s)
    - Can have linked event(s)
    `"]
    AuthorQo22{{"Author: Qo2"}}
    Year2022{{"Year: 2022"}}
    EventArtist{{"Artist: Some artist 1"}}
    end

    subgraph "Some artist 1"
    ArtistPage["`Artist Page
    - Short bio about the artist
    - List of linked blog posts
    - List of linked events
    `"]
    ArtistEvents{{"`Qo2 2022 Event`"}}
    ArtistBlog{{"`Qo2 Radio blog post`"}}
    end
    end


    DefaultBlog --> BlogPost
    AuthorQo2 <-.-> Qo2Blog
    CategryRadio <-.-> RadioBlog

    DefaultCalendar --> Event
    AuthorQo22 <-.-> Qo2Calendar
    Year2022 <-.-> YearCalendar

    Artists --> ArtistPage

    ArtistEvents <-.-> EventArtist
    ArtistBlog <-.-> BlogArtist


```
