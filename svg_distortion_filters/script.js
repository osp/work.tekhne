console.log("hello");

const sliders = document.querySelectorAll(".slider");
sliders.forEach((slider) => {
  slider.addEventListener("input", () => {
    const slider_num = slider.id.split("_")[1];
    const filter = document.querySelector(`#f_${slider_num}_displacement`);
    filter.scale.baseVal = slider.value;
  });
});
