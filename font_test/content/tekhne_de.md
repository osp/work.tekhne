
# tekhné
        
tekhné ist ein gemeinschaftliches Projekt, bei dem sechs europäische Organisationen
das emanzipatorische Potenzial von Technologie in Musik und Klangkunst
Klangkunst. Durch den kritischen und kreativen Umgang mit der Technologie und
indem der Nutzer und nicht der Entwickler in den Mittelpunkt gestellt wird,
können Technologien und Methoden entmystifiziert werden und zu einer
zu einer autonomeren Haltung gegenüber den uns umgebenden Werkzeugen führen.

Viele Momente des sozialen und künstlerischen Wandels wurden in der Geschichte
durch technologische Veränderungen hervorgerufen. Das Projekt nimmt dies als Inspiration und
möchte das Projekt einen Beitrag zu den laufenden Gesprächen über
Technologie und Klangkulturen beitragen. tekhné wird eine Plattform bieten für
Kreative und Kulturschaffende durch eine Kombination aus Forschung,
Experimentieren, Kreation, Bildung und Verbreitung in Form von
Form von Residencies, Auftragsarbeiten, Workshops, Performances
Ausstellungen, Vorträgen, Veröffentlichungen und Radiosendungen.

Das Projekt strebt ein Gleichgewicht zwischen theoretischer Reflexion und
und praktischer Umsetzung, indem es Menschen, die auf
mit verschiedenen Arten von Technologie interagieren wollen. tekhné
zielt darauf ab, ein integrativeres Feld zu unterstützen und Klang und Musik als
ein Mittel zu diesem Zweck.

tekhné wird initiiert von
Q-O2 (BE), CTM Festival (DE),
Skaņu Mežs (LV), OUT.RA (PT),
GMEA (FR), TRAFO (PL).
        