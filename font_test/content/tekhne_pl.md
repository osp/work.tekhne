
# tekhné
        
tekhné to wspólny projekt, w ramach którego sześć europejskich organizacji
ma na celu zbadanie emancypacyjnego potencjału technologii w muzyce i sztuce dźwiękowej.
muzyce i sztuce dźwiękowej. Poprzez krytyczne i kreatywne zaangażowanie w technologię oraz
poprzez skupienie się na użytkowniku, a nie twórcy,
technologie i metodologie mogą zostać zdemistyfikowane i prowadzić do bardziej
autonomicznego podejścia do otaczających nas narzędzi.

Wiele momentów zmian społecznych i artystycznych zostało historycznie
historycznie spowodowanych zmianami technologicznymi. Biorąc to za inspirację, projekt
projekt ma na celu wniesienie wkładu w szersze, toczące się rozmowy wokół
technologii i kultury dźwięku. tekhné zapewni platformę dla
twórców i pracowników kultury poprzez połączenie badań,
eksperymentów, tworzenia, edukacji i rozpowszechniania oferowanych w formie
formie rezydencji, zleceń, warsztatów, występów,
wystaw, rozmów, publikacji i audycji radiowych.

Projekt ma na celu zachowanie równowagi między refleksją teoretyczną a
a praktyczną realizacją, dostarczając doświadczeń ludziom, którzy chcą
interakcji z różnymi rodzajami technologii. tekhné
ma na celu wspieranie bardziej inkluzywnej dziedziny i wykorzystanie dźwięku i muzyki jako narzędzia do tego celu.
dźwięku i muzyki.

tekhné zostało zainicjowane przez
Q-O2 (BE), CTM Festival (DE),
Skaņu Mežs (LV), OUT.RA (PT),
GMEA (FR), TRAFO (PL).
        