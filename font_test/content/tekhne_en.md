
# tekhné
        
tekhné is a collaborative project in which six European organisations
aim to explore the emancipatory potential of technology in music and
sound art. By engaging critically and creatively with technology, and
by putting the focus on the user rather than the developer,
technologies and methodologies can be demystified and lead to more
autonomous mindsets towards the tools around us.

Many moments of social and artistic change have been historically
brought about by shifts in technology. Taking this as inspiration, the
project seeks to contribute to the wider ongoing conversations around
technology and sound cultures. tekhné will provide a platform for
creatives and cultural workers via a combination of research,
experimentation, creation, education, and dissemination offered in the
form of residencies, commissions, workshops, performances,
exhibitions, talks, publications, and radio broadcasts.

The project aims for a balance between theoretical reflection and
practical implementation, providing experiences to people who wish to
interact in inventive ways with different types of technology. tekhné
aims to support a more inclusive field, and to use sound and music as
a vehicle to this end.

tekhné is initiated by
Q-O2 (BE), CTM Festival (DE),
Skaņu Mežs (LV), OUT.RA (PT),
GMEA (FR), TRAFO (PL).
        