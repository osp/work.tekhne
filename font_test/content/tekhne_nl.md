
# tekhné
        
tekhné is een samenwerkingsproject waarin zes Europese organisaties
het emancipatoire potentieel van technologie in muziek en
geluidskunst. Door kritisch en creatief met technologie om te gaan en
door de gebruiker centraal te stellen in plaats van de ontwikkelaar,
kunnen technologieën en methodologieën gedemystificeerd worden en leiden tot een meer
autonome houding ten opzichte van de gereedschappen om ons heen.

Veel momenten van sociale en artistieke verandering zijn historisch
teweeggebracht door verschuivingen in technologie. Dit als inspiratiebron nemend
wil het project bijdragen aan de bredere gesprekken over
technologie en geluidsculturen. tekhné zal een platform bieden voor
creatieven en culturele werkers via een combinatie van onderzoek,
experimentatie, creatie, educatie en verspreiding aangeboden in de
in de vorm van residenties, opdrachten, workshops, optredens,
tentoonstellingen, lezingen, publicaties en radio-uitzendingen.

Het project streeft naar een balans tussen theoretische reflectie en
praktische uitvoering, en biedt ervaringen aan mensen die op inventieve manieren willen
op inventieve manieren willen omgaan met verschillende soorten technologie. tekhné
wil een meer inclusief veld ondersteunen, en geluid en muziek gebruiken als
een middel om dit doel te bereiken.

tekhné is een initiatief van
Q-O2 (BE), CTM Festival (DE),
Skaņu Mežs (LV), OUT.RA (PT),
GMEA (FR), TRAFO (PL).
        