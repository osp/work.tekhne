
# tekhné
        
tekhné é um projeto de colaboração em que seis organizações europeias
europeias têm como objetivo explorar o potencial emancipatório da tecnologia na música e
arte sonora. Ao envolver-se de forma crítica e criativa com a tecnologia, e
colocando o foco no utilizador e não no criador,
tecnologias e metodologias podem ser desmistificadas e conduzir a mentalidades mais autónomas
mais autónomas em relação às ferramentas que nos rodeiam.

Muitos momentos de mudança social e artística foram historicamente
foram historicamente provocados por mudanças na tecnologia. Tomando isto como inspiração, o projeto
projeto procura contribuir para as conversas mais amplas em curso sobre
tecnologia e culturas sonoras. tekhné fornecerá uma plataforma para
criativos e trabalhadores culturais através de uma combinação de investigação,
experimentação, criação, educação e disseminação, sob a forma de
de residências, comissões, workshops, espectáculos
exposições, palestras, publicações e emissões de rádio.

O projeto procura um equilíbrio entre a reflexão teórica e a
e a implementação prática, proporcionando experiências a pessoas que desejam
interagir de forma inventiva com diferentes tipos de tecnologia. tekhné
tem como objetivo apoiar um campo mais inclusivo e usar o som e a música como
como veículo para esse fim.

tekhné é iniciado por
Q-O2 (BE), CTM Festival (DE),
Skaņu Mežs (LV), OUT.RA (PT),
GMEA (FR), TRAFO (PL).
        