
# tekhné
        
tekhné est un projet de collaboration dans lequel six organisations européennes
explorent le potentiel émancipateur de la technologie dans la musique et l'art sonore.
dans la musique et l'art sonore. En s'engageant de manière critique et créative avec la technologie, et
en mettant l'accent sur l'utilisateur plutôt que sur le développeur,
les technologies et les méthodologies peuvent être démystifiées et conduire à des mentalités plus
plus autonomes à l'égard des outils qui nous entourent.

De nombreux moments de changement social et artistique ont été provoqués historiquement par des changements technologiques.
ont été provoqués par des changements technologiques. En s'inspirant de cela, le projet
projet cherche à contribuer aux conversations en cours autour de la technologie et des cultures sonores.
de la technologie et des cultures sonores. tekhné fournira une plateforme pour
créatifs et les travailleurs culturels par le biais d'une combinaison de recherche, d'expérimentation, de création, d'éducation et de diffusion,
d'expérimentation, de création, d'éducation et de diffusion, sous la forme de
sous la forme de résidences, de commandes, d'ateliers, de performances, d'expositions, de conférences, de publications et de radio,
expositions, conférences, publications et émissions de radio.

Le projet vise à trouver un équilibre entre la réflexion théorique et la mise en œuvre pratique.
et la mise en œuvre pratique, en offrant des expériences aux personnes qui souhaitent
qui souhaitent interagir de manière inventive avec différents types de technologies. tekhné
vise à soutenir un domaine plus inclusif, et à utiliser le son et la musique comme
le son et la musique comme moyen d'y parvenir.

tekhné est initié par
Q-O2 (BE), CTM Festival (DE),
Skaņu Mežs (LV), OUT.RA (PT),
GMEA (FR), TRAFO (PL).
        