
# tekhné
        
tekhné ir sadarbības projekts, kurā piedalās sešas Eiropas organizācijas.
mērķis ir izpētīt tehnoloģiju emancipējošo potenciālu mūzikā un mūzikā.
skaņas mākslā. Kritiski un radoši strādājot ar tehnoloģijām, un
koncentrējoties uz lietotāju, nevis izstrādātāju,
tehnoloģiju un metodoloģiju var demistificēt un radīt vairāk
autonomo domāšanas veidu attiecībā uz mums apkārt esošajiem rīkiem.

Daudzi sociālo un māksliniecisko pārmaiņu momenti vēsturiski ir notikuši
ir izraisījušas pārmaiņas tehnoloģiju jomā. Ņemot to par iedvesmas avotu.
projekts cenšas sniegt ieguldījumu plašākās sarunās, kas notiek par tehnoloģiju attīstību.
tehnoloģiju un skaņas kultūru. tekhné nodrošinās platformu
radošajiem un kultūras darbiniekiem, apvienojot pētniecību,
eksperimentēšanu, radīšanu, izglītību un izplatīšanu, kas tiek piedāvāta kultūras un mākslas jomā.
rezidenču, pasūtījumu, darbnīcu un performanču veidā,
izstādes, sarunas, publikācijas un radio pārraides.

Projekta mērķis ir panākt līdzsvaru starp teorētiskām pārdomām un pētniecību.
praktisku īstenošanu, nodrošinot pieredzi cilvēkiem, kuri vēlas
mijiedarboties atjautīgos veidos ar dažāda veida tehnoloģijām. tekhné
mērķis ir atbalstīt iekļaujošāku jomu, kā arī izmantot skaņu un mūziku kā
kā līdzekli šim mērķim.

tekhné iniciatori ir
Q-O2 (BE), CTM Festival (DE),
Skaņu Mežs (LV), OUT.RA (PT),
GMEA (FR), TRAFO (PL).
        