import jinja2
import os

from markdown import markdown
import yaml
import json

import datetime
from dateutil.parser import *

from distutils.dir_util import copy_tree


# OPTIONS
# ------------------------------------------------------------------

CONTENT_PATH = 'content/'
TEMPLATE_PATH = 'template.html'
HTML_PATH = 'www/'
MEDIA_EXTENSION = '.media'


# MARKDOWN EXT
# ------------------------------------------------------------------

# https://python-markdown.github.io/extensions/extra/ 
# (official support for: fenced code block, footnotes)
from markdown.extensions.extra import ExtraExtension

# https://python-markdown.github.io/extensions/toc/
# (official support for TOC)
from markdown.extensions.toc import TocExtension

# https://python-markdown.github.io/extensions/code_hilite/
# (official support for syntax highlighting)
from markdown.extensions.codehilite import CodeHiliteExtension

# https://github.com/funk1d/markdown-figcap
# (third party for figcaption, allow complexe ones with html in it and custom figure content)
from markdown_figcap import FigCapExtension

# https://github.com/evidlo/markdown_captions
# (third party for figcaption, simply transform img alt text into caption)
# NOTE: figure still wrapped in <p>
from markdown_captions import CaptionsExtension

# https://pypi.org/project/markdown3-newtab/
# (third party that make every link into a new tab link)
# NOTE: create a link typology extension
from markdown3_newtab import NewTabExtension


MARKDOWN = {
    'extensions': [
        # ExtraExtension(),
        # CodeHiliteExtension(),
        # FigCapExtension(),
        # FootnoteExtension(),
    ],
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'codelight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': { 'permalink': False },
        'markdown.extensions.footnotes': {}
    }
}


# PARSING
# ------------------------------------------------------------------

def md_parse(NAME_PATH):
    md = ""
    with open(NAME_PATH, 'r') as file:
        md = markdown(file.read(), 
        extensions=MARKDOWN['extensions'], 
        extension_configs=MARKDOWN['extension_configs'])
    return md

def yml_parse(NAME_PATH):
    yml = ""
    with open(NAME_PATH, 'r') as file:
        yml = yaml.safe_load(file)
    return yml


# COLLECTING CONTENT
# ------------------------------------------------------------------

def collect_content():

    # filling a content dictionnary from all the .md or .yml file in the content folder
    possible_extensions = ['.md', '.yml', '.yaml']
    content = {}

    for root, dirs, files in os.walk(CONTENT_PATH):
        for filename in files:
            (basename, ext) = os.path.splitext(filename)
            path = os.path.join(root, filename)
            if ext in possible_extensions:
                
                # items have basename as 'id'
                content[basename] = {
                    'txt': "",
                    'media': []
                }

                # parsing content to 'text' key
                if ext == ".md":
                    content[basename]['txt'] = md_parse(path)
                elif ext == ".yml" or ext == ".yaml":
                    content[basename]['txt'] = yml_parse(path)

                print("{f} parsed".format(f = filename))
                
                # parsing media to 'media' key
                content[basename]['media'] = collect_media(filename)
    
    return content


# COLLECTING MEDIA
# ------------------------------------------------------------------

def path2mediapath(source_path):
    # convert a path of a parsed file to its corresponding media folder (ending with .media)
    filename, file_extension = os.path.splitext(source_path)
    return filename + MEDIA_EXTENSION

def collect_media(filename):

    folder_path = os.path.join("content", path2mediapath(filename))

    # stop if no media folder
    if not os.path.isdir(folder_path):
        return
    
    print('media folder: {f}'.format(f = folder_path))

    # manually copy the folder to the output directory
    save_folder_path = "media/" + path2mediapath(filename)
    copy_tree(folder_path, HTML_PATH + save_folder_path)

    # construct the urls list
    media_urls = []

    # iterate over the media folder
    for media in os.listdir(folder_path):
        if os.path.isfile(os.path.join(folder_path, media)):
            media_urls.append(os.path.join(save_folder_path, media))

    return media_urls


# MAIN
# ------------------------------------------------------------------

if __name__ == '__main__':

    # collect content
    content = collect_content()

    # init jinja environment
    templateLoader = jinja2.FileSystemLoader( searchpath="" )
    env = jinja2.Environment(
        loader=templateLoader
    )

    # getting the template
    template = env.get_template(TEMPLATE_PATH)

    # render template
    html = template.render(content = content)
    with open(HTML_PATH + 'index.html', 'w') as file:
        file.write(html)