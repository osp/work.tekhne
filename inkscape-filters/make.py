import jinja2
import os

# from xml.dom import minidom
import xml.etree.ElementTree as ET

# OPTIONS
# ------------------------------------------------------------------

FAV_FILTERS = [
    # diy filters
    'filter396',
    'filter396-2',
    'filter_3',
    'filter_4',

    # a selection of filter for black and white typography :)
    'f005',
    'f038',
    'f040',
    # 'f046',
    'f060',
    'f077',
    'f086',
    'f087',
    'f164',
    'f093',
    'f006',
    # 'f027',
    # 'f136',
    'f007',
    'f011',
    'f034',
    'f090',
    'f094',
    'f151',
    'f185',
    'f047',
    'f048',
    'f049',
    'f051',
    'f066',
    'f067',
    'f068',
    'f070',
    'f020',
    # 'f033',
    'f101',
    'f102',
    'f108',
    'f138',
    'f144',
    'f156',
    'f037',
    'f043',
    'f054',
    'f057',
    # 'f059',
    'f188',
    'f075',
    'f227',
    'f231'
]

FILTERS_PATHS = ['custom_filters.svg', 'inkscape_filters.svg']
TEMPLATE_PATH = 'template-interface.html'
HTML_PATH = ''
IMG_PATH = []

options = {
    'title': 'svg filter from inkscape',
    'description': 'svg filter from inkscape', 
    'print': False,
    'reset': True,
}

with os.scandir('images/') as imgs:
    for img in imgs:
        IMG_PATH.append('images/' + img.name)


# MAIN
# ------------------------------------------------------------------

if __name__ == '__main__':

    ns = '{http://www.w3.org/2000/svg}'
    nsi = '{http://www.inkscape.org/namespaces/inkscape}'

    filters = {}

    for filters_file in FILTERS_PATHS:

        tree = ET.parse(filters_file)
        root = tree.getroot()

        for filter in root.iter(ns+'filter'):
            
            # an inkscape category OR '00-self-made'
            category = 'DIY'
            if nsi+'menu' in filter.attrib:
                category = filter.attrib[nsi+'menu']

            # add category if not already in
            if category not in filters:
                filters[category] = []
            
            # add filter to category
            filters[category].append(filter)

            # write label
            label = filter.attrib['id']
            if nsi+'label' in filter.attrib:
                label = filter.attrib[nsi+'label']

            print(label + ' #' + filter.attrib['id'] + " [" + category + "]")

            s = []
            for e in filter:
                s.append(ET.tostring(e, encoding="unicode"))

            filter.attrib['code'] = ''.join(s)
            filter.attrib['code'] = filter.attrib['code'].replace('ns0:', '')
            filter.attrib['code'] = filter.attrib['code'].replace('xmlns:ns0="http://www.w3.org/2000/svg"', '')

            filter.attrib['label'] = label


    # init jinja environment
    templateLoader = jinja2.FileSystemLoader( searchpath="" )
    env = jinja2.Environment(
        loader=templateLoader
    )

    # getting the template
    template = env.get_template(TEMPLATE_PATH)

    # render template
    html = template.render(ET = ET, filters = filters, options = options, FILTERS_PATHS = FILTERS_PATHS, FAV_FILTERS = FAV_FILTERS, IMG_PATH = IMG_PATH)
    with open(HTML_PATH + 'index.html', 'w') as file:
        file.write(html)
