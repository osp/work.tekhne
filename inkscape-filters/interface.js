
// for select, slider, etc
let root = document.documentElement;

// default filters selected
let default_values = [
  // 'f011',
  'filter_3',
  'f057',
  'f094',
  'f007',
  // 'f231',
  'filter396',
  'f108',
]

let logos = ['.logo__display', '.logo__mini'];

// --- SELECTS

function updateFilter(slct){
  let index = slct.name.split('-')[1] - 1;
  let filter_id = slct.value;

  for (let logo_class of logos){
    let logo_spans = document.querySelectorAll(logo_class + ' span');
  
    let logo_span = logo_spans[index];
    console.log(index, logo_span.innerHTML, filter_id);
    
    logo_span.style.filter = "url(#" + filter_id + ")";
  }

  // let about_spans = document.querySelectorAll('.about-text span');
  // let about_span = about_spans[index];
  // about_span.style.filter = "url(#" + filter_id + ")";
}

let selects = document.getElementsByTagName("select");

for(let slct of selects){

  let index = slct.name.split('-')[1] - 1;
  slct.value = default_values[index];

  slct.addEventListener('input', function(){
    updateFilter(slct);
  });
  
  updateFilter(slct);
}

// --- CSS SLIDERS

let css_sliders = document.getElementsByClassName('css-slider');

function css_update_prop(prop, value){
  // update a css custom property
  root.style.setProperty(prop, value);
}

function css_slider_prop_update(slider){
    // read current value of a slider,
    // and update it's tied prop
    let prop = slider.dataset.prop;
    let unit = slider.hasAttribute('data-unit') ? slider.dataset.unit : "";
    css_update_prop(prop, slider.value + unit);
}

for(let slider of css_sliders){
  slider.addEventListener('input', function(){
      css_slider_prop_update(slider);
  });
  css_slider_prop_update(slider);
}

// CHECKBOXES
// --------------------------------------------------------

let class_checkboxes = document.getElementsByClassName('class-checkbox');

function toggle_class(classname, val){
  if(val){
      document.body.classList.add(classname);
  }
  else{
      document.body.classList.remove(classname);
  }
}
for(let checkbox of class_checkboxes){
  let classname = checkbox.dataset.class;
  checkbox.addEventListener('input', function(){
      toggle_class(classname, checkbox.checked);
  });
  toggle_class(classname, checkbox.checked);
}

// --- SVG SLIDERS

let svg_sliders = document.getElementsByClassName('svg-slider');

function svg_slider_prop_update(slider){
  // read current value of a slider,
  // and update it's tied prop
  let primitive_name = slider.dataset.primitive;
  let prop = slider.dataset.prop;

  // let primitives = document.getElementsByTagNameNS("http://www.w3.org/2000/svg", "feDisplacementMap");
  let primitives = document.querySelectorAll("feDisplacementMap");

  primitives.forEach((primitive) => {
    primitive.setAttribute("scale", slider.value);
  });
}

for(let slider of svg_sliders){
  slider.addEventListener('input', function(){
      svg_slider_prop_update(slider);
  });
  svg_slider_prop_update(slider);
}

// --- BUTTONS

let random_filter_button = document.getElementsByClassName('random-filter-button')[0];

random_filter_button.addEventListener('click', function(){

  for(let slct of selects){
    let items = slct.getElementsByTagName('option');
    let index = Math.floor(Math.random() * items.length);
    slct.selectedIndex = index;

    updateFilter(slct);
  }
  
});